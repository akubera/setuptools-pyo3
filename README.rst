===============
setuptools-pyo3
===============

Add setup.cfg configuration to setuptools-rust_ project

Licensed under the `MIT software license`_.

.. _setuptools-rust: https://pypi.org/project/setuptools-rust/
.. _MIT software license: http://gitlab.com/akubera/setuptools-pyo3/blob/LICENSE
